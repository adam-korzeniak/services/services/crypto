package com.adamkorzeniak.crypto.api.rest;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CryptoAssetRequest {
    private String name;
    private String symbol;
    private String externalId;
    private BigDecimal amount;
    private String location;
}
