package com.adamkorzeniak.crypto.api.rest;

import java.math.BigDecimal;

record CryptoAssetResponse(
        String name,
        String symbol,
        BigDecimal amount,
        String location) {
}