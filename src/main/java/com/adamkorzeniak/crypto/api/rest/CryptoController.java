package com.adamkorzeniak.crypto.api.rest;

import com.adamkorzeniak.crypto.domain.CryptoAssetsFacade;
import com.adamkorzeniak.crypto.domain.dto.CryptoAssetDetailsDto;
import com.adamkorzeniak.crypto.domain.dto.CryptoAssetDto;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api/crypto")
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class CryptoController {

    private final CryptoAssetsFacade cryptoAssetsFacade;
    private final CryptoApiMapper cryptoApiMapper;

    @GetMapping
    ResponseEntity<CryptoAssetsDetailsResponse> getCryptoAssets(
            @RequestParam(defaultValue = "PLN") String currency,
            @RequestParam(defaultValue = "value") String sort) {
        List<CryptoAssetDetailsDto> cryptoAssets = cryptoAssetsFacade.getCryptoAssets(currency, sort);
        return new ResponseEntity<>(cryptoApiMapper.mapDetailsResponse(cryptoAssets, currency), HttpStatus.OK);
    }

    @PostMapping
    ResponseEntity<CryptoAssetResponse> createCryptoAsset(@Valid @RequestBody CryptoAssetRequest request) {
        CryptoAssetDto cryptoAsset = cryptoApiMapper.mapRequest(request);
        CryptoAssetDto result = cryptoAssetsFacade.createCryptoAsset(cryptoAsset);
        return new ResponseEntity<>(cryptoApiMapper.mapResponse(result), HttpStatus.OK);
    }

    @PostMapping("/many")
    ResponseEntity<List<CryptoAssetResponse>> createManyCryptoAssets(@RequestBody List<CryptoAssetRequest> request) {
        List<CryptoAssetDto> cryptoAssets = cryptoApiMapper.mapRequest(request);
        List<CryptoAssetDto> result = cryptoAssetsFacade.createCryptoAssets(cryptoAssets);
        return new ResponseEntity<>(cryptoApiMapper.mapResponse(result), HttpStatus.OK);
    }
}
