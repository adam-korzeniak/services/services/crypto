package com.adamkorzeniak.crypto.api.rest;

import java.math.BigDecimal;

record CryptoAssetDetailsResponse(
        String name,
        String symbol,
        Integer rank,
        String platform,
        BigDecimal maxSupply,
        BigDecimal totalSupply,
        BigDecimal price,
        BigDecimal marketCap,
        BigDecimal amount,
        BigDecimal value,
        String location) {
}