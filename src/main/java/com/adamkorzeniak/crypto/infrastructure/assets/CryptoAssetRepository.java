package com.adamkorzeniak.crypto.infrastructure.assets;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface CryptoAssetRepository extends JpaRepository<CryptoAssetEntity, Long> {
}
