package com.adamkorzeniak.crypto.infrastructure.assets;

import com.adamkorzeniak.crypto.domain.dto.CryptoAssetDto;

import java.util.List;

public interface CryptoAssetService {
    List<CryptoAssetDto> getAll();

    CryptoAssetDto create(CryptoAssetDto cryptoAsset);

    List<CryptoAssetDto> create(List<CryptoAssetDto> cryptoAssets);
}
