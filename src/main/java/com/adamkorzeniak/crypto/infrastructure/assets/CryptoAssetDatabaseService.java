package com.adamkorzeniak.crypto.infrastructure.assets;

import com.adamkorzeniak.crypto.domain.dto.CryptoAssetDto;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class CryptoAssetDatabaseService implements CryptoAssetService {

    private final CryptoAssetRepository cryptoAssetRepository;
    private final CryptoAssetMapper cryptoAssetMapper;

    @Override
    public List<CryptoAssetDto> getAll() {
        List<CryptoAssetEntity> assets = cryptoAssetRepository.findAll();
        return cryptoAssetMapper.mapToDtos(assets);
    }

    @Override
    public CryptoAssetDto create(CryptoAssetDto cryptoAsset) {
        CryptoAssetEntity entity = cryptoAssetMapper.mapToEntity(cryptoAsset);
        return cryptoAssetMapper.mapToDto(cryptoAssetRepository.save(entity));
    }

    @Override
    public List<CryptoAssetDto> create(List<CryptoAssetDto> cryptoAssets) {
        List<CryptoAssetEntity> entities = cryptoAssetMapper.mapToEntities(cryptoAssets);
        return cryptoAssetMapper.mapToDtos(cryptoAssetRepository.saveAll(entities));
    }
}
