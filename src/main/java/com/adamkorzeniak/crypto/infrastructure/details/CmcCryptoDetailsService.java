package com.adamkorzeniak.crypto.infrastructure.details;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Set;

@Service
@RequiredArgsConstructor
class CmcCryptoDetailsService {

    private final RestTemplate restTemplate;
    private final EndpointConfig endpointConfig;
    private final ObjectMapper objectMapper;

    CMCCryptoResponse getCryptoDetails(Set<String> cmcIds, String currency) {
        URI uri = buildURI(cmcIds, currency);
        HttpEntity<Void> entity = buildHttpEntity();
        try {
            return restTemplate.exchange(uri, HttpMethod.GET, entity, CMCCryptoResponse.class).getBody();
        } catch (HttpClientErrorException exc) {
            try {
                CMCErrorResponse errorResponse = objectMapper.readValue(exc.getResponseBodyAsString(), CMCErrorResponse.class);
                throw new RuntimeException("CMC server replied with " + errorResponse);
            } catch (JsonProcessingException e) {
                throw exc;
            }
        }
    }

    private URI buildURI(Set<String> cmcIds, String currency) {
        return UriComponentsBuilder.fromUriString(endpointConfig.getCmc().getUrl())
                .path(endpointConfig.getCmc().getQuotesPath())
                .queryParam("id", String.join(",", cmcIds))
                .queryParam("convert", currency)
                .build().toUri();
    }

    private HttpEntity<Void> buildHttpEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.set(endpointConfig.getCmc().getAuthHeader(), endpointConfig.getCmc().getAuthHeaderValue());
        return new HttpEntity<>(null, headers);
    }
}
