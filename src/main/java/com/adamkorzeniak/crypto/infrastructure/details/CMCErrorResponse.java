package com.adamkorzeniak.crypto.infrastructure.details;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.OffsetDateTime;

@Data
class CMCErrorResponse {

    private Error status;

    @Data
    static class Error {

        private OffsetDateTime timestamp;

        private long elapsed;

        @JsonProperty("error_code")
        private String errorCode;

        @JsonProperty("error_message")
        private String errorMessage;

        @JsonProperty("credit_count")
        private Integer creditCount;

        private String notice;
    }

    @Override
    public String toString() {
        return "status code: " + status.errorCode + ", message: " + status.errorMessage;
    }

}
