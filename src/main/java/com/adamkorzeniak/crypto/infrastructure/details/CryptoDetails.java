package com.adamkorzeniak.crypto.infrastructure.details;

import java.math.BigDecimal;

public record CryptoDetails(
        String name,
        String symbol,
        Integer rank,
        String platform,
        BigDecimal maxSupply,
        BigDecimal totalSupply,
        BigDecimal price,
        BigDecimal marketCap) {
}
