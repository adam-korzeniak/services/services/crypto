package com.adamkorzeniak.crypto.infrastructure.details;

import com.adamkorzeniak.crypto.infrastructure.details.dto.CryptosDetailsDto;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
class CmcCryptoDetailsProvider implements CryptoDetailsProvider {

    private final CmcCryptoDetailsService cryptoDetailsService;
    private final CmcMapper cmcMapper;

    @Cacheable(cacheNames = "cryptoDetails")
    public CryptosDetailsDto getCryptoDetails(Set<String> externalIds, String currency) {
        CMCCryptoResponse cmcResponse = cryptoDetailsService.getCryptoDetails(externalIds, currency);
        return cmcMapper.mapDetails(cmcResponse, currency);
    }
}
