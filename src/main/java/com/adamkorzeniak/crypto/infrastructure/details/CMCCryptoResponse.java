package com.adamkorzeniak.crypto.infrastructure.details;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;

@Data
class CMCCryptoResponse {

    private Map<String, CMCCrypto> data = Collections.emptyMap();

    Map<String, CMCCrypto> getCryptos() {
        return data;
    }

    @Data
    static class CMCCrypto {

        private String name;
        private String symbol;
        @JsonProperty("cmc_rank")
        private Integer cmcRank;
        private Platform platform;

        @JsonProperty("max_supply")
        private BigDecimal maxSupply;
        @JsonProperty("total_supply")
        private BigDecimal totalSupply;

        private Map<String, CMCCryptoQuote> quote = Collections.emptyMap();
    }

    @Data
    static class Platform {
        private String name;
        private String symbol;
    }

    @Data
    static class CMCCryptoQuote {
        private BigDecimal price;
        @JsonProperty("market_cap")
        private BigDecimal marketCap;
    }
}
