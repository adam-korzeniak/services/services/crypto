package com.adamkorzeniak.crypto.infrastructure.details.dto;

import com.adamkorzeniak.crypto.infrastructure.details.CryptoDetails;
import lombok.AllArgsConstructor;

import java.util.Collections;
import java.util.Map;

@AllArgsConstructor
public class CryptosDetailsDto {
    private final Map<String, CryptoDetails> data;

    public CryptosDetailsDto() {
        this.data = Collections.emptyMap();
    }

    public CryptoDetails get(String key) {
        return data.get(key);
    }
}
